import java.util.ArrayList;
import java.util.List;

public class Contact {
    private String name;
    private List<String> contactNumbers;
    private List<String> addresses;

    // Default constructor
    public Contact() {
        this.name = "";
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }

    // Parameterized constructor
    public Contact(String name) {
        this.name = name;
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }

    // Getter and setter methods for name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Getter and setter methods for contactNumbers
    public List<String> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(List<String> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    // Getter and setter methods for addresses
    public List<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<String> addresses) {
        this.addresses = addresses;
    }

    // Method to add a contact number
    public void addContactNumber(String contactNumber) {
        contactNumbers.add(contactNumber);
    }

    // Method to add an address
    public void addAddress(String address) {
        addresses.add(address);
    }
}
