import java.util.ArrayList;
import java.util.List;

public class Phonebook {
    private List<Contact> contacts; // ArrayList of Contact objects

    // Default constructor
    public Phonebook() {
        contacts = new ArrayList<>();
    }

    // Parameterized constructor to initialize contacts with an existing list
    public Phonebook(List<Contact> contacts) {
        this.contacts = new ArrayList<>(contacts);
    }

    // Getter method to get the list of contacts
    public List<Contact> getContacts() {
        return contacts;
    }

    // Setter method to add a contact to the phonebook
    public void addContact(Contact contact) {
        contacts.add(contact);
    }
}
